section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    ._loop:
        cmp byte [rdi + rax], 0
        je ._end
        inc rax
        jmp ._loop
    ._end:
ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
ret

; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov byte [rsp], dil
    mov rdx, 1
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    syscall
    inc rsp
ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 0xA
    call print_char
ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, 10
    xor r8, r8
    ._loop:
        xor rdx, rdx
        div rdi
        add rdx, "0"
        dec rsp
        inc r8
        mov byte [rsp], dl
        cmp rax, 0
        jne ._loop
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, r8
    syscall
    add rsp, r8
ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .found_minus
    .end_found_minus:
        call print_uint
ret
.found_minus: 
    neg rdi
    mov r9, rdi
    mov dil, "-"
    call print_char
    mov rdi, r9
    jmp .end_found_minus


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    xchg rdi, rsi
    mov rdx, rax
    call string_length
    cmp rdx, rax
    jne .not_equal
    xor rdx, rdx
    ._loop:
        mov cl, byte [rdi + rdx]
        cmp cl, byte [rsi + rdx]
        jne .not_equal
        inc rdx
        cmp rdx, rax
        jb ._loop
    mov rax, 1
ret
.not_equal:
    xor rax, rax
ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov ah, al
    mov al, byte [rsp]
    inc rsp
ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    dec rsi
    push rsi
    push rdi

    xor r8, r8
    xor r9, r9
    ._loop: 
        call read_char
        test r8, r8
        jnz .ncheck

        test ah, ah
        je ._e_loop_1
        cmp al, 0x20
        je ._loop
        cmp al, 0x9
        je ._loop
        cmp al, 0xa
        je ._loop
        test al, al
        je ._e_loop_1
        inc r8
        .ncheck:
        test ah, ah
        je ._e_loop_1
        cmp al, 0x20
        je ._e_loop_1
        cmp al, 0x9
        je ._e_loop_1
        cmp al, 0xa
        je ._e_loop_1
        test al, al
        je ._e_loop_1
        inc r9
        mov rdi, qword [rsp+8]
        cmp r9, rdi
        ja ._e_loop_2

        mov rdi, qword [rsp]
        mov byte [rdi+r9-1], al
        xor al, al
        jmp ._loop

        ._e_loop_1:
            pop rax
            add rsp, 8
            mov byte [rax+r9], 0
            mov rdx, r9
ret
        ._e_loop_2:
            add rsp, 16
            xor rax, rax
ret  
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: ; di - str
    xor r9, r9 ; len
    xor rsi, rsi ; number
    mov rcx, 1   ; multiplier
    ._searchloop:
        cmp byte [rdi+r9], '0'
        jae ._upper_check
        jmp ._end_sloop

    ._calcloop: ; r9 - numbers len 
        xor rax, rax
        mov al, byte [rdi+r9-1]
        sub al, '0'
        xor rdx, rdx
        mul rcx
        add rsi, rax
        lea rcx, [rcx*4+rcx]
        shl rcx, 1
        dec r9
        jnz ._calcloop

        ._end:
            mov rax, rsi
            pop rdx
        ret

._upper_check:
    cmp byte [rdi+r9], '9'
    ja ._end_sloop
    inc r9
    jmp ._searchloop

._error:
    xor rax, rax
    xor rdx, rdx
ret

._end_sloop:
    test r9, r9
    jz ._error
    push r9
jmp ._calcloop




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], "-"
    je .found_minus
    call parse_uint
ret 
.found_minus:
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .end
    neg rax
    inc rdx
ret
.end:
ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: 
    call string_length
    inc rax 
    cmp rax, rdx
    ja .end
    mov rcx, rax 
    cld
    xchg rsi, rdi
    rep movsb
    ret
.end:
    xor rax, rax 
ret
